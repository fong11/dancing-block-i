
//  
// Example code for OpenGL programming
//
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <ctime>
#include <SOIL/SOIL.h>
#include <math.h>
//pi for the sine animation calculations
#define PI 3.141592653 
//set the fps to 30
int nFPS = 30;

//to change the rotating angle
int fRotateAngle = 0;

clock_t startClock=0,curClock;
long long int prevF=0,curF=0;
//for displaying the triangle lines
int dipMode=1;
//these vertices can change for the animation
float vertices[] = {-.6,1, 0,1, .6,1,
					-.6,.6, -.2,.6, .2,.6, .6,.6,
					-.6,-.6, -.2,-.6, .2,-.6, .6,-.6,
					-.6,-1, .6,-1};

//these vertices are a duplicate of the ones above, used for animation.
float staticvertices[] = {-.6,1, 0,1, .6,1,
					-.6,.6, -.2,.6, .2,.6, .6,.6,
					-.6,-.6, -.2,-.6, .2,-.6, .6,-.6,
					-.6,-1, .6,-1};

//vertices are ordered top to bottom, left to right. Similar to a book. 

//top half
short vertorder1[] = {1,0,3,4,8,9,5,6,2};
//bottom block
short vertorder2[] = {11,7,8,9,10,12};


//these two are set to determine if r or s was clicked
bool rotate = false;
bool moving = false;

//value used for animation
float sine[360];
int movingnumber = 0;

//set of 12 colors for rendering
float colors[] = {1,0,0, 0,0,1, 0,1,1,
				  0,0,1, 1,0,0, 0,1,0,
				  0,1,1, 1,1,0, 1,0,0,
				  0,1,0, 0,0,1, 1,0,0};
//used for animation. vertex numbers
short top[] = {0,2,3,6};
short bottom[] = {7,10,11,12};

void init(void)
{
	// init your data, setup OpenGL environment here
	glClearColor(0.9,0.9,0.9,1.0); // clear color is gray		
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // uncomment this function if you only want to draw wireframe model
					// GL_POINT / GL_LINE / GL_FILL (default)
	glPointSize(4.0);

	//calculating sin values
	for (int i = 0; i < 360; i++) 
		sine[i] = sin(PI/180 * i);
}

void display(void)
{
	if(dipMode==1)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}else{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	

	curF++;
	// put your OpenGL display commands here
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// reset OpenGL transformation matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); // reset transformation matrix to identity

	// setup look at transformation so that 
	// eye is at : (0,0,3)
	// look at center is at : (0,0,0)
	// up direction is +y axis
	gluLookAt(0.f,0.f,3.f,0.f,0.f,0.f,0.f,1.f,0.f);
	glRotatef(fRotateAngle,.4,.6,1);


	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	
	//set vertex and color pointers
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glColorPointer(3, GL_FLOAT, 0, colors);

	//draw
	glDrawElements(GL_TRIANGLE_FAN, 9, GL_UNSIGNED_SHORT, vertorder1);
	glDrawElements(GL_TRIANGLE_FAN, 6, GL_UNSIGNED_SHORT, vertorder2);

	glutSwapBuffers();	// swap front/back framebuffer to avoid flickering 

	//displays the fps set at the top. 
	curClock=clock();
	float elapsed=(curClock-startClock)/(float)CLOCKS_PER_SEC;
	if(elapsed > 1.0f){
		float fps=(float)(curF-prevF)/elapsed;
		printf("fps:%f\n",fps);
		prevF=curF;
		startClock=curClock;
	}
}
 
void reshape (int w, int h)
{
	// reset viewport ( drawing screen ) size
	glViewport(0, 0, w, h);
	float fAspect = ((float)w)/h; 
	// reset OpenGL projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70.f,fAspect,0.001f,30.f); 
}



void keyboard(unsigned char key, int x, int y)
{
	// put your keyboard control here
	if (key == 27) 
	{
		// ESC hit, so quit
		printf("demonstration finished.\n");
		exit(0);
	}
	//displays the triangle lines
	if( key == 'h'){
		dipMode = 1-dipMode;
	}
	//animation moves the top and bottom blocks in separate directions
	else if(key == 's'){
		moving = !moving;
	}
	//rotates the block I
	else if(key == 'r'){
		rotate = !rotate;
	}
}

void mouse(int button, int state, int x, int y)
{
	// process your mouse control here
	//displays this information when left clicked
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		printf("push left mouse button.\n");
}


void timer(int v)
{
	//changes the rotating angle.
	if(rotate){
		fRotateAngle += 2; 
		fRotateAngle %= 360;
	}
	//changes the animation
	if(moving){
		movingnumber += 2;
		movingnumber %= 360;
	}
	// change rotation angles
	glutPostRedisplay(); // trigger display function by sending redraw into message queue
	glutTimerFunc(1000/nFPS,timer,v); // restart timer again

	//calculation to change the position of the vertices. sine calculation done in the init. 
	for(int i = 0; i<4; i++){
		vertices[2*top[i]] = staticvertices[2*top[i]]+.4*sine[movingnumber];
		vertices[2*bottom[i]] = staticvertices[2*bottom[i]]-.4*sine[movingnumber];
	}
}

int main(int argc, char* argv[])
{
	glutInit(&argc, (char**)argv);
	// set up for double-buffering & RGB color buffer & depth test
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); 
	glutInitWindowSize (500, 500); 
	glutInitWindowPosition (100, 100);
	glutCreateWindow ((const char*)"MP 1");

	init(); // setting up user data & OpenGL environment
	
	// set up the call-back functions 
	glutDisplayFunc(display);  // called when drawing 
	glutReshapeFunc(reshape);  // called when change window size
	glutKeyboardFunc(keyboard); // called when received keyboard interaction
	glutMouseFunc(mouse);	    // called when received mouse interaction
	glutTimerFunc(100,timer,nFPS); // a periodic timer. Usually used for updating animation
	
	startClock=clock();

	glutMainLoop(); // start the main message-callback loop

	return 0;
}

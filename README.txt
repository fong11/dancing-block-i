CS 418 MP1 README

by:
Michael Fong
fong11

To open the project you'll need Microsoft Visual Studio 2013. 
Open the mp1.sln file within the Source folder to run the project. 

Controls:
r : rotate
s : animate top and bottom parts of the block I
h : display triangle lines

Youtube Video Demo:
http://www.youtube.com/watch?v=S9CUOsPgusI&feature=youtu.be